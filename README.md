# NetFlix OSS + Spring boot example


## Presentation

In this example we will build a Micro service architecture using Netflix OSS and Spring Boot

## Development

Pull the project and access import the maven projects into your IDE, and inside every project execute the following command : 

    //windows
	mvnw spring-boot:run

	//linux and mac
	./mvnw spring-boot:run

